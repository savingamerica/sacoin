#!/bin/bash

# Helper script to build a release build + archive. Run from top-level repository directory.
# Requires:
# PATH=$PATH:/path/to/Qt/5.2.1/clang_64/bin
# QTDIR=/path/to/Qt/5.2.1/clang_64
# CODESIGNARGS='--sign "Developer ID Application: ..." --deep'

# Dependencies
if [ ! -e "/Applications/Xcode.app" ]; then
    echo "Couldn't find XCode, make sure it's installed."
    exit 1
fi

if [ ! -e "/usr/local/opt/boost" ]; then
    echo "Couldn't find boost, make sure it's installed."
    exit 1
fi

if [ ! -e "/usr/local/opt/berkeley-db" ]; then
    echo "Couldn't find Berkely DB, make sure it's installed."
    exit 1
fi

if [ ! -e "/usr/local/opt/openssl" ]; then
    echo "Couldn't find OpenSSL, make sure it's installed."
    exit 1
fi

if [ ! -e "/usr/local/opt/miniupnpc" ]; then
    echo "Couldn't find miniupnpc, make sure it's installed."
    exit 1
fi

if [ -z `which qmake` ]; then
    echo "Couldn't find qmake, make sure Qt is installed and you've set your PATH to access it."
    exit 1
fi

# NOTE: No good way to test appscript...


echo "Resetting hard, trying to clear out previous build output to make sure we're in a clean state"
git reset --hard HEAD
if [ -e "Makefile" ]; then
    make distclean
fi
# Remove some other by-products
rm -rf *.dmg SACoin-Qt.app

echo "Applying project file patch for Homebrew"
patch -p1 < contrib/homebrew/bitcoin.qt.pro.patch

echo "Building..."
qmake RELEASE=1 USE_UPNP=1 && \
    make clean && \
    make && \
    T=$(contrib/qt_translations.py $QTDIR/translations src/qt/locale) && \
    python2.7 share/qt/clean_mac_info_plist.py && \
    python2.7 contrib/macdeploy/macdeployqtplus SACoin-Qt.app -add-qt-tr $T -dmg -fancy contrib/macdeploy/fancy.plist -sign
