#!/bin/bash

if [ ! -e "miniupnpc-1.9.20140401.tar.gz" ]; then
    wget 'http://miniupnp.free.fr/files/download.php?file=miniupnpc-1.9.20140401.tar.gz' -O 'miniupnpc-1.9.20140401.tar.gz'
fi
if [ ! -e "openssl-1.0.1h.tar.gz" ]; then
    wget 'http://www.openssl.org/source/openssl-1.0.1h.tar.gz'
fi
if [ ! -e "db-5.3.28.NC.tar.gz" ]; then
    wget 'http://download.oracle.com/berkeley-db/db-5.3.28.NC.tar.gz'
fi
if [ ! -e "zlib-1.2.8.tar.gz" ]; then
    wget 'http://zlib.net/zlib-1.2.8.tar.gz'
fi
if [ ! -e "libpng-1.6.8.tar.gz" ]; then
    wget 'ftp://ftp.simplesystems.org/pub/libpng/png/src/history/libpng16/libpng-1.6.8.tar.gz'
fi
if [ ! -e "qrencode-3.4.3.tar.bz2" ]; then
    wget 'http://fukuchi.org/works/qrencode/qrencode-3.4.3.tar.bz2'
fi
if [ ! -e "boost_1_55_0.tar.bz2" ]; then
    wget 'http://downloads.sourceforge.net/project/boost/boost/1.55.0/boost_1_55_0.tar.bz2'
fi
if [ ! -e "boost-mingw-gas-cross-compile-2013-03-03.patch" ]; then
    wget 'http://wtogami.fedorapeople.org/boost-mingw-gas-cross-compile-2013-03-03.patch'
fi
if [ ! -e "qt-everywhere-opensource-src-4.8.5.tar.gz" ]; then
    wget 'http://pkgs.fedoraproject.org/repo/pkgs/qt/qt-everywhere-opensource-src-4.8.5.tar.gz/1864987bdbb2f58f8ae8b350dfdbe133/qt-everywhere-opensource-src-4.8.5.tar.gz'
fi
