#!/bin/bash

# A helper script that automates the process described in doc/release-process.md
# for building Win32 binaries. This assumes you've already followed a few steps:
#
# 1. You're on a reasonable Ubuntu platform, e.g. a modern 64-bit host. Note
# that despite similarities, Debian will not work.
# 2. You've already tagged the release
# 3. You have sacoin.git checked out at sacoin/ and this script will run with
# write-permissions to the parent directory of sacoin/
#
# Usage: From the top-level sacoin/ directory:
#
#   ./contrib/gitian-builder/build.sh VERSION COMMIT
#
# Note that it seems to not be unusual to have some issues with
# timeouts when VMs are booting, so you may have to run this more than
# once to get it to finish successfully. Additionally, in order to get
# permissions right, some commands are executed as sudo, which may
# leave some annoying permissions issues when you need to clean up.

set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

VERSION="$1"
if [ -z "$VERSION" ]; then
    echo "You need to pass a version to this script."
    exit 1
fi

COMMIT="$2"
if [ -z "$COMMIT" ]; then
    echo "You need to pass the exact commit hash to this script."
    exit 1
fi

echo "Making sure we have necessary dependencies"
sudo apt-get install -y git apache2 apt-cacher-ng python-vm-builder ruby qemu-utils qemu-kvm zip

# Check to make sure we have gitian-builder, clone if not
pushd ../
if [ ! -e "gitian-builder" ]; then
    echo "Cloning missing gitian-builder repository"
    git clone https://github.com/devrandom/gitian-builder.git
fi

pushd gitian-builder

if [ ! -e "base-precise-amd64.qcow2" ]; then
    echo "Building base builder VM"
    sudo ./bin/make-base-vm --suite precise --arch amd64
fi


mkdir -p inputs;
pushd inputs/
echo "Fetching any missing inputs"
$DIR/download-inputs.sh
popd


if [ ! -e "inputs/boost-win32-1.55.0-gitian-r6.zip" ]; then
    echo "Building Boost dependency"
    sudo ./bin/gbuild ../sacoin/contrib/gitian-descriptors/boost-win32.yml
    cp build/out/boost-*.zip inputs/
fi

if [ ! -e "inputs/bitcoin08-deps-win32-gitian-r11.zip" ]; then
    echo "Building Win32 dependency"
    sudo ./bin/gbuild ../sacoin/contrib/gitian-descriptors/deps-win32.yml
    cp build/out/bitcoin*.zip inputs/
fi

if [ ! -e "inputs/qt-win32-4.8.5-gitian-r6.zip" ]; then
    echo "Building Qt-Win32 dependency"
    sudo ./bin/gbuild ../sacoin/contrib/gitian-descriptors/qt-win32.yml
    cp build/out/qt*.zip inputs/
fi

# Unlike dependencies, we always build this so we can force an
# update. The above only need forced updates if the version changes,
# in which case file names should change.
echo "Building Win32 sacoin binaries"
sudo ./bin/gbuild --commit sacoin=${COMMIT} ../sacoin/contrib/gitian-descriptors/gitian-win32.yml
#./bin/gsign --signer $SIGNER --release ${VERSION}-win32 --destination ../gitian.sigs/ ../sacoin/contrib/gitian-descriptors/gitian-win32.yml
pushd build/out
sudo zip -r sacoin-${VERSION}-${COMMIT}-win32.zip *
popd
popd
popd

cp ../gitian-builder/build/out/sacoin-${VERSION}-${COMMIT}-win32.zip .
