SA Coin
===================

http://www.savingamerica.com

* Copyright (c) 2009-2014 Bitcoin Developers
* Copyright (c) 2011-2014 Litecoin Developers
* Copyright (c) 2014 Saving America Inc.


What is SA Coin?
----------------

SA Coin is a distributed, peer-to-peer digital currency. It
is secure because it uses cryptographic techniques to track every transaction
made with SA Coin; it is controlled by its users because the system is driven by
the network of users without any central authority; and it is efficient, both
because transactions can be created and confirmed quickly and because they can
be made nearly for free.

The code for SA Coin is open source, which means anyone can read it, modify it,
compile it, redistribute it, and use it to participate in the SA Coin
network. Allowing anyone to modify it may sound dangerous, but the design of
SA Coin prevents modified SA Coin clients from creating invalid transactions:
unless the network agrees a transaction is valid, it will not be accepted.


SA Coin Design
--------------

SA Coin is based on the same design, and code as Bitcoin and its descendant,
Litecoin. The system has three key features which ensure continuing and correct
functioning.

1. Consensus-based Blockchain Network - the core of the system is a distributed
   data structure called the blockchain, which represents the entire history of
   transactions ever performed. Each block describes a set of transactions,
   using the previous block as a reference point. Each block is also
   cryptographically secure, which means it is not possible to generate invalid
   blocks and trick the network into thinking they are valid. This works because
   of the proof-of-work scheme (described below) which makes it effectively
   impossible to generate fake transactions that the network will accept.

   Because the blockchain is public and records the entire history of
   transactions, it is possible for anyone to determine the balance of any
   SA Coin account (called an address) and see all transactions to and from that
   account. However, accounts are also anonymous because they are based on
   cryptographic private/public key pairs. This allows the network as a whole to
   verify every single transaction without revealing any personal account
   information.

2. Proof of Work - The blockchain records the history, but participants also
   need to be able to create new blocks to apply new transactions. If any user
   could do this at any time, the network would never be able to agree on the
   next block because there would be many competing candidates. Instead, new
   blocks can only be created by recording a set of transactions into a record,
   then performing a cryptographically difficult computation on that
   record. That computation proves that the participant has done a certain
   amount of work (statistically). When that computation is completed, that
   candidate is broadcast to the network and other nodes can verify the blocks
   validity. This verification is very cheap (in contrast to finding the valid
   block in the first place, which represents the proof of work). Assuming the
   block is valid, the network will quickly agree it is valid, accept it, and
   move on to computing the next bock.

   Because the total computational power of the network changes over time, the
   difficulty of the work that must be performed is adjusted over time as
   well. This ensures that new blocks are created, on average, at a fixed
   rate. This is determined automatically and agreed upon by the network, so it
   is similarly immune to most attacks by parties that only represent a small
   fraction of the entire network.

3. Mining Reward - The continued functioning of the blockchain requires a
   network of participants to support it. Further, the network must not be
   controlled by a single entity simply by running enough clients such that it
   surpasses 50% of the network. Therefore, an incentive is added to make
   participation worthwhile even if the participant is not making transactions:
   each block created includes a reward delivered to the creator's account. This
   process is called "mining" because the process of participating and creating
   blocks results in reward SA Coin. The cost of mining is that the participant
   must accept requests for transfers, create blocks from them, and perform the
   proof of work to create a new block on the blockchain.

   The mining reward does not remain constant. If it did, the total available
   coin would grow indefinitely at a fixed rate. Instead, the reward for
   creating a block is reduced periodically. Over time, this reward reaches a
   small enough value that rewards stop. At this point, the SA Coin network
   should be self-sufficient: anyone with SA Coin who will eventually want to use
   it (via a transfer on the blockchain) will have a vested interest in
   maintaining the blockchain and network.

Combined, these three features ensure that the system allows secure, nearly free
exchanges of coin, that invalid transactions cannot be made, and that the system
continues to run and is supported indefinitely by miners.


SA Coin Characteristics
-----------------------

These features are also shared by Bitcoin, Litecoin, and other altcoins. The
SA Coin algorithms and code are directly derived from the Litecoin code and
therefore many properties are similar. The following properties differ:

* Addresses use a different scheme, starting with 't', to avoid conflicting with
  other major coins
* Blocks complete approximately every 2 minutes. This rate means transactions
  can often be performed in 2 minutes, weakly confirmed in an hour, and strongly
  confirmed in 3 hours.
* Difficulty is recomputed once per day (every 720 blocks).
* The eventual total number of SA Coin is approximately 100 billion. The reward
  for each mined block is very large for the first few blocks, immediately
  putting about half of the coin in circulation. It subsequently follows a
  common halving scheme: the reward starts at 25,000 coin per block and halves
  approximately every 4 years. The much larger per-block reward compared to
  Litecoin results in the much larger total number of coin.


Using SA Coin
-------------

You can find instructions for building and running the software included in this
repository in the `docs/` directory.

For more information, as well as an immediately useable, binary version of
the SA Coin client sofware, see http://www.savingamerica.com.

Contact
-------
Please contact logan@savingamerica.com with questions.

License
-------

SA Coin is released under the terms of the MIT license. See `COPYING` for more
information or see http://opensource.org/licenses/MIT.
